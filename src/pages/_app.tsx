/* istanbul ignore file */
import '../styles.css';

import * as React from 'react';

import {CacheProvider, EmotionCache} from '@emotion/react';

import CssBaseline from '@mui/material/CssBaseline';
import Head from 'next/head';
import {AppProps as NextAppProps} from 'next/app';
import {ThemeProvider} from '@mui/material/styles';
import {UserProvider} from '@utils/auth/use-user';
import createEmotionCache from '../createEmotionCache';
import theme from '../theme';

// Client-side cache, shared for the whole session of the user in the browser.
const clientSideEmotionCache = createEmotionCache();

interface AppProps extends NextAppProps {
    emotionCache?: EmotionCache;
}

export default function _App(props: AppProps) {
    const {Component, emotionCache = clientSideEmotionCache, pageProps} = props;
    return (
        <CacheProvider value={emotionCache}>
            <Head>
                <title>Okami quiz</title>
                <meta name="viewport" content="initial-scale=1, width=device-width" />
            </Head>
            <ThemeProvider theme={theme}>
                <UserProvider>
                    {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
                    <CssBaseline />
                    <Component {...pageProps} />
                </UserProvider>
            </ThemeProvider>
        </CacheProvider>
    );
}
