import {
    Alert,
    Box,
    Button,
    OutlinedInput,
    Snackbar,
    Typography
} from '@mui/material';

import Layout from '@components/layout';
import {useRouter} from 'next/router';
import {useState} from 'react';

const Home = () => {
    const {query} = useRouter();
    const [isSnackErrorOpen, setIsSnackErrorOpen] = useState(true);
    return (<Layout>
        <Box sx={{
            backgroundColor: 'background.default',
            display: 'flex',
            flexDirection: 'column',
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center'
        }}>
            <Box component="form" sx={{
                backgroundColor: 'background.paper',
                display: 'grid',
                gridTemplateColumns: 'auto',
                gridGap: 1,
                padding: 2,
                textAlign: 'center'
            }}>
                <Typography component="h1" variant="h4">Okami quiz</Typography>
                <OutlinedInput
                    sx={{textAlign: 'center'}}
                    type="text"
                    placeholder="Code PIN du quiz"
                    name="code"
                    required
                    tabIndex={0} />
                <Button type="submit" variant="contained" color="primary">REJOINDRE</Button>
            </Box>
        </Box>
        {query.error && (
            <Snackbar open={isSnackErrorOpen} autoHideDuration={6000} onClose={() => setIsSnackErrorOpen(false)}>
                <Alert elevation={6} variant="filled" severity="error">{query.error}</Alert>
            </Snackbar>
        )}
    </Layout>);
};

export default Home;
