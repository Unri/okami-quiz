import {NextApiRequest, NextApiResponse} from 'next';
import {serializeFaunaCookie, serverClient} from '@utils/fauna';

import {ServerResponse} from 'http';
import {query as q} from 'faunadb';
import {requestUser} from '@lib/discord/request';

interface LoginResult {
    secret: string;
}

export default function login(req: NextApiRequest, res: NextApiResponse) {
    // Not public api
    res.status(404).end();
}

/**
 * Send a login request
 * @param code - Authorization code
 * @param res - Server response
 * @throws if an error occur on authentificateUser or it no secret login found
 * @returns if login success, return the user, else undefined
 */
export const requestLogin = async (code: string, res: ServerResponse) => {
    if (!code)
        return undefined;

    const user = await requestUser(code);
    if (!user.verified) {
        throw Error('Cannot identify unverified user');
    }

    try {
        await serverClient.query(
            q.If(q.Exists(q.Match(q.Index('userById'), user.userId)),
                q.Update(q.Collection('User'), {
                    credentials: {password: user.userId},
                    data: user
                }),
                q.Create(q.Collection('User'), {
                    credentials: {password: user.userId},
                    data: user
                })
            )
        );
    } catch (error) {
        console.error('Fauna create user error:', error);
    }

    const loginRes = await serverClient.query<LoginResult>(
        q.Login(q.Match(q.Index('userById'), user.userId), {
            password: user.userId
        })
    );

    if (!loginRes.secret)
        throw new Error('No secret present in login query response.');

    const cookieSerialized = serializeFaunaCookie(loginRes.secret);

    res.setHeader('Set-Cookie', cookieSerialized);
    return user;
};
