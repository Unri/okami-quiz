import {NextApiRequest, NextApiResponse} from 'next';
import {faunaClient, faunaQueryGraphQL, getAuthTokenFromRequest} from '@utils/fauna';

import {query as q} from 'faunadb';

export const quizListApi = async (token: string) => {
    const userRef = await faunaClient(token).query<{id: string}>(q.CurrentIdentity());
    const query = `query {
        findUserByID(id: "${userRef.id}") {
            quiz {
                data {
                    _id
                    name
                    questions {
                        data {
                            _id
                            statement
                            options
                            media
                            answers
                            type
                            theme
                            value
                            duration
                        }
                    }
                }
            }
        }
    }`;
    return faunaQueryGraphQL(query)
        .then(res => res?.data?.findUserByID?.quiz?.data)
        .then(res =>
            res?.map(({questions, _id: quizId, ...quizProps}) => ({
                id: quizId,
                ...quizProps,
                questions: questions.data.map(({_id: questionId, ...questionProps}) => ({
                    id: questionId,
                    ...questionProps
                }))
            }))
        );
};

export default async function quizList(req: NextApiRequest, res: NextApiResponse) {
    if (req.method !== 'GET')
        return res.status(404).end();

    const token = getAuthTokenFromRequest(req);

    if (!token)
        return res.status(401).send('Auth cookie missing.');

    res.status(200).json(await quizListApi(token));
}
