import {NextApiRequest, NextApiResponse} from 'next';
import {faunaClient, getAuthTokenFromRequest} from '@utils/fauna';

import {IncomingMessage} from 'http';
import {User} from '@models/user';
import {query as q} from 'faunadb';

export default async function auth(req: NextApiRequest, res: NextApiResponse) {
    if (req.method !== 'GET')
        return res.status(404).end();

    const result = await checkUserAuthentificated(req);
    result
        ? res.status(200).json(result)
        : res.status(401).send('Auth cookie missing.');
}

/**
 * Check if the user is authentificated
 * @param req - Incoming message request
 * @returns if authentificated, return the authentificated user, else undefined
 */
export const checkUserAuthentificated = async (req: IncomingMessage) => {
    const token = getAuthTokenFromRequest(req);

    if (!token)
        return undefined;

    const {data: user} = await faunaClient(token).query<{data: User}>(q.Get(q.CurrentIdentity()));
    return user;
};
