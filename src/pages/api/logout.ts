import {FAUNA_SECRET_COOKIE, faunaClient, getAuthTokenFromRequest} from '@utils/fauna';
import {NextApiRequest, NextApiResponse} from 'next';

import cookie from 'cookie';
import {query as q} from 'faunadb';

export default async function logout(req: NextApiRequest, res: NextApiResponse) {
    if (req.method !== 'POST')
        return res.status(404).end();

    const token = getAuthTokenFromRequest(req);
    if (!token) {
        // Already logged out.
        return res.status(200).end();
    }
    // Invalidate secret (ie. logout from Fauna).
    await faunaClient(token).query(q.Logout(false));
    // Clear cookie.
    const cookieSerialized = cookie.serialize(FAUNA_SECRET_COOKIE, '', {
        sameSite: 'lax',
        secure: process.env.NODE_ENV === 'production',
        maxAge: -1,
        httpOnly: true,
        path: '/'
    });
    res.setHeader('Set-Cookie', cookieSerialized);
    res.status(200).end();
}
