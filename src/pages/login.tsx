import {authUrl} from '@lib/discord/auth';
import {checkUserAuthentificated} from './api/auth';
import {requestLogin} from './api/login';

const redirectProps = (destination: string) => ({redirect: {destination, permanent: false}});

export const getServerSideProps = async ({query, req, res}) => {
    if (await checkUserAuthentificated(req))
        return redirectProps('/');

    if (query.code) {
        try {
            await requestLogin(query.code as string, res);
            return redirectProps('/');
        } catch (error) {
            return redirectProps(`/?${new URLSearchParams({error})}`);
        }
    }
    return redirectProps(authUrl);
};

function Login() {
    // Because getServerSideProps redirect in any case
    return null;
}

export default Login;
