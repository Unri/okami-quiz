import Box from '@mui/system/Box';
import Layout from '@components/layout';
import LoadingSpinner from '@components/loading-spinner';
import {Quiz} from '@models/quiz';
import QuizItemCard from '@components/quiz-item-card';
import {Typography} from '@mui/material';
import useSWR from 'swr';
import {withAuthRequired} from '@utils/auth/with-auth-required';

const QuizList = () => {
    const {data, error} = useSWR<Quiz[]>('/api/quiz-list');
    return (<Layout>
        {!!data && (
            <Box component="ul" sx={{paddingLeft: 5, paddingRight: 5}}>
                {data.map(({id, name, questions}, i) =>
                    <Box
                        key={id}
                        component="li"
                        sx={{listStyle: 'none', marginTop: i === 0 ? 0 : 1}}
                    >
                        <QuizItemCard
                            title={name}
                            subtitle={`${questions.length} questions`}
                            imgUrl={questions[0]?.media ?? '/werewolf.png'}
                            imgAlt="Quiz image" />
                    </Box>
                )}
            </Box>
        )}
        {data?.length === 0 && <Typography component="h3" align="center">No quiz found</Typography>}
        {!data && <LoadingSpinner />}
        {JSON.stringify(error)}
    </Layout>);
};

export default withAuthRequired(QuizList);
