import {FC, ReactElement} from 'react';

import {NextRouter} from 'next/router';
import {RouterContext} from 'next/dist/shared/lib/router-context';
import {ThemeProvider} from '@mui/material/styles';
import {User} from '@models/user';
import {UserProvider} from '../../utils/auth/use-user';
import {isEmpty} from 'ramda';
import theme from '../../theme';

const defaultMockRouter: NextRouter = {
    pathname: '/',
    route: '/',
    query: {},
    asPath: '/',
    basePath: '/',
    isLocaleDomain: true,
    push: jest.fn(),
    replace: jest.fn(),
    reload: jest.fn(),
    back: jest.fn(),
    prefetch: jest.fn().mockResolvedValue(undefined),
    beforePopState: jest.fn(),
    events: undefined,
    isFallback: false,
    isReady: true,
    isPreview: false
};

const MockedRouterProvider = ({children, ...mockedrouterProps}) => (
    <RouterContext.Provider value={{...defaultMockRouter, ...mockedrouterProps}}>
        {children}
    </RouterContext.Provider>
);

const MockedThemeProvider = ({children}) => (
    <ThemeProvider theme={theme}>
        {children}
    </ThemeProvider>
);

export type SetupTestContextProps = {
    hasRouter?: boolean;
    hasTheme?: boolean;
    hasAuth?: boolean;
    mockedRouterProps?: Partial<NextRouter>;
    authUser?: User;
    children?: ReactElement;
}

export const SetupTestContext = ({
    hasRouter = false,
    hasTheme = false,
    hasAuth = false,
    mockedRouterProps = {},
    authUser,
    children: contextChildren
}: SetupTestContextProps) => {
    const providerList: [boolean, FC][] = [
        [hasAuth || !!authUser, ({children}) => <UserProvider user={authUser}>{children}</UserProvider>],
        [hasTheme, MockedThemeProvider],
        [hasRouter || !isEmpty(mockedRouterProps), ({children}) =>
            <MockedRouterProvider {...mockedRouterProps}>{children}</MockedRouterProvider>]
    ];
    return providerList.reduce((acc, [has, Provider]) => has ? <Provider>{acc}</Provider> : acc, contextChildren);
};
