import {createContext, useCallback, useContext, useEffect, useState} from 'react';

import {User} from '@models/user';

interface UserContextProps {
    user?: User
    error?: Error
    isLoading: boolean
    checkSession: () => Promise<void>;
    logout: () => Promise<void>;
}

const missingUserProvider = 'You forgot to wrap your app in <UserProvider>';

const UserContext = createContext<UserContextProps>({
    get user(): never {
        throw new Error(missingUserProvider);
    },
    get error(): never {
        throw new Error(missingUserProvider);
    },
    get isLoading(): never {
        throw new Error(missingUserProvider);
    },
    checkSession: (): never => {
        throw new Error(missingUserProvider);
    },
    logout: (): never => {
        throw new Error(missingUserProvider);
    }
});

interface UserProviderState {
    user?: User;
    error?: Error
    isLoading: boolean;
}

export const useUser = () => useContext(UserContext);

export const UserProvider = ({
    children,
    user: initialUser = undefined,
    authUrl = '/api/auth',
    logoutUrl = '/api/logout'
}) => {
    const [state, setState] = useState<UserProviderState>({user: initialUser, isLoading: !initialUser});

    const checkSession = useCallback(async () => {
        try {
            const response = await fetch(authUrl);
            const user = response.ok ? await response.json() : undefined;
            setState(previous => ({...previous, user, error: undefined}));
        } catch (e) {
            const error = new Error(`The request to ${authUrl} failed`);
            setState(previous => ({...previous, user: undefined, error}));
        }
    }, [authUrl]);

    const logout = useCallback(async () => {
        setState(previous => ({...previous, isLoading: true}));
        await fetch(logoutUrl, {
            method: 'POST',
            credentials: 'include',
            headers: {
                'Access-Control-Allow-Origin': window.location.origin
            }
        });
        // To trigger logout on other tabs
        window.localStorage.setItem('logout', Date.now().toString());
        setState({user: undefined, error: undefined, isLoading: false});
    }, [logoutUrl]);

    // Synchronize logout on every tabs
    const syncLogout = useCallback((event: StorageEvent) => {
        if (event.key === 'logout')
            checkSession();
    }, [checkSession]);

    useEffect(() => {
        window.addEventListener('storage', syncLogout);

        return () => {
            window.removeEventListener('storage', syncLogout);
            window.localStorage.removeItem('logout');
        };
    }, [syncLogout]);

    useEffect(() => {
        if (state.user) return;
        checkSession()
            .finally(() => setState(previous => ({...previous, isLoading: false})));
    }, [state.user, checkSession]);

    return (
        <UserContext.Provider value={{...state, checkSession, logout}}>{children}</UserContext.Provider>
    );
};

