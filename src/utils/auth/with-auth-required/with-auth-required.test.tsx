import {render, waitFor} from '@testing-library/react';

import {SetupTestContext} from '@utils/test/setup-test-context';
import {User} from '@models/user';
import fetchMock from 'jest-fetch-mock';
import {withAuthRequired} from './index';

const testId = 'children-component';
const ChildrenComponent = withAuthRequired(() => <div data-testid={testId} />);

describe('With auth required wrapper', () => {
    beforeEach(() => {
        fetchMock.doMock();
    });

    afterEach(() => {
        fetchMock.resetMocks();
    });

    it('render children component when authentificated', async () => {
        const mockedPushMethod = jest.fn();
        const mockedUser: User = {userId: 'id', username: 'Username', email: 'email@email.com'};
        const {getByTestId} = render(
            <SetupTestContext
                authUser={mockedUser}
                mockedRouterProps={{push: mockedPushMethod}}>
                <ChildrenComponent />
            </SetupTestContext>);

        await waitFor(() => {
            expect(getByTestId(testId)).toBeInTheDocument();
            expect(mockedPushMethod).not.toHaveBeenCalled();
        });
    });

    it('render an error when authentification failed', async () => {
        fetchMock.mockResponse(() => Promise.reject());
        const mockedPushMethod = jest.fn();
        const {queryByTestId, getByText} = render(
            <SetupTestContext
                hasAuth
                mockedRouterProps={{push: mockedPushMethod}}>
                <ChildrenComponent />
            </SetupTestContext>);

        await waitFor(() => {
            expect(queryByTestId(testId)).not.toBeInTheDocument();
            expect(getByText(/Error/)).toBeInTheDocument();
            expect(mockedPushMethod).toHaveBeenCalled();
        });
    });

    it('render nothing on fallback', async () => {
        fetchMock.mockResponseOnce(undefined, {status: 500});
        const mockedPushMethod = jest.fn();
        const {queryByTestId} = render(
            <SetupTestContext
                hasAuth
                mockedRouterProps={{push: mockedPushMethod}}>
                <ChildrenComponent />
            </SetupTestContext>);

        await waitFor(() => {
            expect(queryByTestId(testId)).not.toBeInTheDocument();
            expect(mockedPushMethod).toHaveBeenCalled();
        });

    });
});
