import {useEffect} from 'react';
import {useRouter} from 'next/router';
import {useUser} from '../use-user';

const defaultOnRedirecting = () => <></>;

const defaultOnError = (error: Error) => <div>Error: {error.message}</div>;

export const withAuthRequired = Component => props => {
    const router = useRouter();
    const {user, error, isLoading} = useUser();

    useEffect(() => {
        if ((user && !error) || isLoading) return;

        router.push('/login');
    }, [user, error, isLoading, router]);

    if (error) return defaultOnError(error);
    if (user) return <Component {...props} />;

    return defaultOnRedirecting();
};
