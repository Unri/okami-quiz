import {IncomingMessage} from 'http';
import cookie from 'cookie';
import faunadb from 'faunadb';

export const FAUNA_SECRET_COOKIE = 'faunaSecret';

export const serverClient = new faunadb.Client({
    secret: process.env.FAUNA_SERVER_KEY
});

// Used for any authed requests.
export const faunaClient = (secret: string) => new faunadb.Client({secret});

export const serializeFaunaCookie = (userSecret: string) => cookie.serialize(FAUNA_SECRET_COOKIE, userSecret, {
    sameSite: 'lax',
    secure: process.env.NODE_ENV === 'production',
    maxAge: 72576000,
    httpOnly: true,
    path: '/'
});

export const faunaQueryGraphQL = (query: string) => fetch('https://graphql.fauna.com/graphql', {
    method: 'POST',
    headers: {
        Authorization: `Bearer ${process.env.FAUNA_SERVER_KEY}`,
        'Content-Type': 'application/json',
        Accept: 'application/json'
    },
    body: JSON.stringify({query})
}).then(res => res.json());

export const getAuthTokenFromRequest = (request: IncomingMessage) => {
    const parsedCookie = cookie.parse(request.headers.cookie ?? '');
    return parsedCookie[FAUNA_SECRET_COOKIE];
};
