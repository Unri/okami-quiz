import {createTheme} from '@mui/material/styles';

const theme = createTheme({
    palette: {
        mode: 'dark',
        background: {
            default: 'hsl(214, 28%, 4.9%)',
            paper: 'hsl(215, 21%, 11%)'
        }
    },
    spacing: (factor: number) => `${0.8 * factor}rem`
});

export default theme;
