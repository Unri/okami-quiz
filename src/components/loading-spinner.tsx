const LoadingSpinner = () => (<>
    <div className="loading-spinner" />
    <style jsx>{`
        :root {
            spinner-size: 3rem;
        }
        @keyframes spining {
            to { transform:  rotate(360deg);}
        }

        .loading-spinner {
            position: absolute;
            display: inline-block;
            width: 3rem;
            height: 3rem;
            border: 3px solid rgba(255,255,255,.3);
            border-radius: 50%;
            border-top-color: var(--color-text-primary);
            top: calc(50% - 3rem);
            right: calc(50% - 3rem);
            animation: spining 1s ease-in-out infinite;
        }
    `}</style>
</>);

export default LoadingSpinner;
