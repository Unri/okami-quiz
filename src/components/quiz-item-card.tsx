import * as React from 'react';

import {Card, CardContent, CardMedia, Typography} from '@mui/material';

import {Box} from '@mui/system';

type QuizItemCardProps = {
    title: string;
    subtitle?: string;
    imgUrl?: string;
    imgAlt: string;
};

const QuizItemCard: React.FC<QuizItemCardProps> = ({title, subtitle, imgUrl, imgAlt}) => (
    <Card tabIndex={0} sx={{
        display: 'flex',
        cursor: 'pointer',
        userSelect: 'none',
        ':hover': {backgroundImage: 'none'}
    }}>
        <CardMedia
            component="img"
            sx={{width: 151, height: 151, p: 1, display: {xs: 'none', sm: 'initial'}}}
            image={imgUrl}
            alt={imgAlt}
        />
        <Box sx={{display: 'flex', flexDirection: 'column'}}>
            <CardContent sx={{flex: '1 0 auto'}}>
                <Typography component="div" variant="h5">
                    {title}
                </Typography>
                <Typography variant="subtitle1" color="text.secondary" component="div">
                    {subtitle}
                </Typography>
            </CardContent>
        </Box>
    </Card>
);

export default QuizItemCard;
