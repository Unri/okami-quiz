import Box from '@mui/material/Box';
import {FC} from 'react';
import Header from './header';
import {useUser} from '@utils/auth/use-user';

const Layout: FC = ({children}) => {
    const {user, isLoading, logout} = useUser();
    return (
        <Box component="main" sx={{
            display: 'flex',
            flexDirection: 'column',
            height: '100%',
            width: '100%'
        }}>
            <Header
                isLoggedIn={!isLoading && !!user}
                isLoggedOut={!isLoading && !user}
                onLogout={logout}/>
            {children}
        </Box>
    );
};

export default Layout;
