import {AppBar, Button, Link, Toolbar} from '@mui/material';
import React, {HTMLAttributes} from 'react';

import Image from 'next/image';
import {default as NextLink} from 'next/link';

type HeaderProps = HTMLAttributes<HTMLElement> & {
    isLoggedIn?: boolean;
    isLoggedOut?: boolean;
    onLogout: () => void;
}

const Header = ({
    isLoggedIn = false,
    isLoggedOut = false,
    onLogout
}: HeaderProps) => (
    <AppBar position="static" sx={{backgroundImage: 'none'}}>
        <Toolbar>
            <NextLink href="/">
                <Link sx={{
                    cursor: 'pointer',
                    display: 'flex',
                    justifyContent: 'center',
                    marginRight: 'auto'
                }}>
                    <Image
                        alt="Header home logo"
                        src="/werewolf.png"
                        width={56}
                        height={56}
                    />
                </Link>
            </NextLink>
            {isLoggedIn && !isLoggedOut && (<>
                <NextLink href="/quiz-list">
                    <Button color="inherit">Mes quiz</Button>
                </NextLink>
                <Button color="inherit" onClick={onLogout}>Déconnexion</Button>
            </>)}
            {!isLoggedIn && isLoggedOut && (
                <NextLink href="/login">
                    <Button color="inherit">Connexion</Button>
                </NextLink>
            )}
        </Toolbar>
    </AppBar>
);

export default Header;
