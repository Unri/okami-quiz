import Header from './index';
import {render} from '@testing-library/react';
import userEvent from '@testing-library/user-event';

describe('header component', () => {
    it('has a login button when not logged in', () => {
        const {getByRole, queryByRole} = render(<Header isLoggedOut onLogout={jest.fn()} />);

        expect(getByRole('button', {name: 'Connexion', hidden: false})).toBeInTheDocument();
        expect(queryByRole('button', {name: 'Déconnexion', hidden: false})).not.toBeInTheDocument();
    });

    it('has a logout button when logged in', () => {
        const {getByRole, queryByRole} = render(<Header isLoggedIn onLogout={jest.fn()} />);

        expect(getByRole('button', {name: 'Déconnexion', hidden: false})).toBeInTheDocument();
        expect(queryByRole('button', {name: 'Connexion', hidden: false})).not.toBeInTheDocument();
    });

    it('trigger logout when click on logout button', () => {
        const mockLogoutHandler = jest.fn();
        const {getByRole} = render(<Header isLoggedIn onLogout={mockLogoutHandler} />);

        userEvent.click(getByRole('button', {name: 'Déconnexion', hidden: false}));

        expect(mockLogoutHandler).toHaveBeenCalled();
    });
});
