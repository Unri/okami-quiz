const {
    DISCORD_ID,
    DISCORD_SECRET,
    NEXTAUTH_URL
} = process.env;

export const credentials = {
    client_id: DISCORD_ID,
    client_secret: DISCORD_SECRET,
    grant_type: 'authorization_code',
    redirect_uri: `${NEXTAUTH_URL}/login`,
    scope: 'identity'
};

type Token = {
    access_token: string;
    token_type: string;
    expires_in: number;
    refresh_token: string;
    scope: string;
};

const requestToken = (code: string): Promise<Token> =>
    fetch('https://discord.com/api/oauth2/token', {
        method: 'POST',
        body: (new URLSearchParams({...credentials, code})).toString(),
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })
        .then(res => res.json())
        .then(res => {
            if (res.error) {
                throw new Error(res.error_description);
            }
            return res;
        });

type DiscordUser = {
    userId: string;
    username: string;
    email?: string;
    verified: boolean;
}

const getUser = ({token_type, access_token}: Token): Promise<DiscordUser> =>
    fetch('https://discord.com/api/users/@me', {
        headers: {
            authorization: `${token_type} ${access_token}`
        }
    })
        .then(res => res.json())
        .then(({id, username, email, verified}) => ({userId: id, username, email, verified}));

/**
 * Request the authentificated Discord user from authentification code
 * @param code Authentification code
 * @throws Error on discord fetch calls error
 * @returns the authentificated user
 */
export const requestUser = (code: string) => requestToken(code).then(getUser);
