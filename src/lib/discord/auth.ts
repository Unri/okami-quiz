const params = {
    client_id: process.env.DISCORD_ID,
    redirect_uri: `${process.env.NEXTAUTH_URL}/login`,
    response_type: 'code',
    scope: 'email'
};

const baseUrl = 'https://discord.com/api/oauth2/authorize';

export const authUrl = `${baseUrl}?${new URLSearchParams(params).toString()}`;
