export enum QuestionType {
    ShortText = 'ShortText',
    LongText = 'LongText',
    SingleChoice = 'SingleChoice'
}

export interface Question {
    id: string;
    /** Question statement */
    statement: string;
    /** Options for choice type question*/
    options?: string[];
    /** Correct answer(s) of the question */
    answers: string[];
    /** Type of question */
    type: QuestionType;
    /** Main theme */
    theme: string;
    /** Media url */
    media?: string;
    /** Value of the question */
    value: number;
    /** Duration (in seconds) */
    duration: number;
}
