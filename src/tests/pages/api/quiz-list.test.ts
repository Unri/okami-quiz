import * as R from 'ramda';

import {FAUNA_SECRET_COOKIE} from '@utils/fauna';
import {NextApiRequest} from 'next';
import quizListAPIHandler from '../../../pages/api/quiz-list';

const expectedResponse = [{
    id: 'id_quiz',
    name: 'name',
    questions: [{
        id: 'id_question',
        statement: 'statement',
        options: ['opt'],
        media: 'url',
        answers: ['answer'],
        type: 'type',
        theme: 'theme',
        value: 1,
        duration: 10
    }]
}];

// To bypass type check on mocked request and response
const quizListHandler = (req, res) => quizListAPIHandler(req, res);

describe('API /quiz-list handler', () => {
    beforeEach(() => {
        fetchMock.resetMocks();
    });
    it.each(['POST', 'UPDATE', 'DELETE'])('returns status 404 if the request method is not GET', async method => {
        const statusSpy = jest.fn();
        const mockResponse = {status: (status: number) => {
            statusSpy(status);
            return {end: jest.fn()};
        }};
        await quizListHandler({method} as NextApiRequest, mockResponse);
        expect(statusSpy).toBeCalledWith(404);
    });

    it('returns status 401 when not logged', async () => {
        const statusSpy = jest.fn();
        const mockResponse = {status: (status: number) => {
            statusSpy(status);
            return {send: jest.fn()};
        }};
        await quizListHandler({method: 'GET', headers: {}}, mockResponse);
        expect(statusSpy).toBeCalledWith(401);
    });

    it('returns status 200 with formated quizList', async () => {
        fetchMock.mockResponseOnce(() => Promise.resolve(JSON.stringify({
            resource: {
                data: {
                    userId: 'id',
                    username: 'Username',
                    email: 'email@email.email'
                }
            }
        })));
        const mockedAPIResponse =
        `{"data":{"findUserByID":{"quiz":{"data":[{"_id":"id_quiz","name":"name","questions":${JSON.stringify({
            data: [{
                _id: 'id_question',
                ...R.omit(['id'], expectedResponse[0].questions[0])
            }]
        })}}]}}}}`;
        fetchMock.mockResponseOnce(() => Promise.resolve(mockedAPIResponse));
        const statusSpy = jest.fn();
        const responseSpy = jest.fn();
        const mockResponse = {
            status: (status: number) => {
                statusSpy(status);
                return {json: responseSpy};
            }
        };
        const mockRequest = {
            method: 'GET',
            headers: {
                cookie: `${FAUNA_SECRET_COOKIE}=secret`
            }
        };
        await quizListHandler(mockRequest, mockResponse);
        expect(statusSpy).toHaveBeenCalledWith(200);
        expect(responseSpy).toHaveBeenCalledWith(expectedResponse);
    });
});

