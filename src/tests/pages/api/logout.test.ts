import {FAUNA_SECRET_COOKIE} from '@utils/fauna';
import {NextApiRequest} from 'next';
import cookie from 'cookie';
import logoutAPIHandler from '../../../pages/api/logout';

// To bypass type check on mocked request and response
const logoutHandler = (req, res) => logoutAPIHandler(req, res);

describe('API /logout handler', () => {
    it.each(['GET', 'UPDATE', 'DELETE'])('returns status 404 if the request method is not POST', async method => {
        const statusSpy = jest.fn();
        const mockResponse = {status: (status: number) => {
            statusSpy(status);
            return {end: jest.fn()};
        }};
        await logoutHandler({method} as NextApiRequest, mockResponse);
        expect(statusSpy).toBeCalledWith(404);
    });

    it('returns status 200 when already logged out', async () => {
        const statusSpy = jest.fn();
        const mockResponse = {status: (status: number) => {
            statusSpy(status);
            return {end: jest.fn()};
        }};
        await logoutHandler({method: 'POST', headers: {}} as NextApiRequest, mockResponse);
        expect(statusSpy).toBeCalledWith(200);
    });

    it('returns status 200 with deleted cookie on logout', async () => {
        fetchMock.mockResponseOnce(() => Promise.resolve('{}'));
        const statusSpy = jest.fn();
        let newCookie = '';
        const mockResponse = {
            status: (status: number) => {
                statusSpy(status);
                return {end: jest.fn()};
            },
            setHeader: (_: string, headerCookie: string) => {
                newCookie = headerCookie;
            }
        };
        const mockRequest = {
            method: 'POST',
            headers: {
                cookie: `${FAUNA_SECRET_COOKIE}=secret`
            }
        };
        await logoutHandler(mockRequest, mockResponse);
        expect(statusSpy).toBeCalledWith(200);
        expect(Number(cookie.parse(newCookie)['Max-Age'])).toBeLessThan(0);
    });
});

