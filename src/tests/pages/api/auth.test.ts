import {FAUNA_SECRET_COOKIE} from '@utils/fauna';
import {NextApiRequest} from 'next';
import {User} from '@models/user';
import authAPIHandler from '../../../pages/api/auth';
import fetchMock from 'jest-fetch-mock';

// To bypass type check on mocked request and response
const authHandler = (req, res) => authAPIHandler(req, res);

describe('API /auth handler', () => {
    beforeEach(() => {
        fetchMock.doMock();
    });

    afterEach(() => {
        fetchMock.resetMocks();
    });
    it.each(['POST', 'UPDATE', 'DELETE'])('returns 404 status if the request method is not GET', async method => {
        const statusSpy = jest.fn();
        const mockResponse = {status: (status: number) => {
            statusSpy(status);
            return {end: jest.fn()};
        }};
        await authHandler({method} as NextApiRequest, mockResponse);
        expect(statusSpy).toBeCalledWith(404);
    });

    it('returns 401 with error message if auth cookie is missing', async () => {
        const statusSpy = jest.fn();
        const resultSpy = jest.fn();

        const mockResponse = {status: (status: number) => {
            statusSpy(status);
            return {send: resultSpy};
        }};
        const mockRequest = {
            method: 'GET',
            headers: {}
        };

        await authHandler(mockRequest, mockResponse);

        expect(statusSpy).toBeCalledWith(401);
        expect(resultSpy).toBeCalledWith('Auth cookie missing.');
    });

    it('returns the authentificated user with auth cookie', async () => {
        const mockUser: User = {userId: 'id', username: 'Username', email: 'email@email.com'};
        fetchMock.mockResponseOnce(() => Promise.resolve(JSON.stringify({
            resource: {data: mockUser}
        })));
        const statusSpy = jest.fn();
        const resultSpy = jest.fn();

        const mockResponse = {status: (status: number) => {
            statusSpy(status);
            return {json: resultSpy};
        }};
        const mockRequest = {
            method: 'GET',
            headers: {cookie: `${FAUNA_SECRET_COOKIE}=secret`}
        };

        await authHandler(mockRequest, mockResponse);

        expect(statusSpy).toBeCalledWith(200);
        expect(resultSpy).toBeCalledWith(mockUser);
    });
});
