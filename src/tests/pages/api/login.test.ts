import authAPIHandler from '../../../pages/api/login';

// To bypass type check on mocked request and response
const loginHandler = (req, res) => authAPIHandler(req, res);

describe('API /login handler', () => {
    it('returns status 404', () => {
        const statusSpy = jest.fn();
        const mockResponse = {status: (status: number) => {
            statusSpy(status);
            return {end: jest.fn()};
        }};
        loginHandler({}, mockResponse);
        expect(statusSpy).toBeCalledWith(404);
    });
});
