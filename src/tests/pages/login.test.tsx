import LoginPage, {getServerSideProps} from '../../pages/login';

import {FAUNA_SECRET_COOKIE} from '@utils/fauna';
import {authUrl} from '@lib/discord/auth';
import cookie from 'cookie';
import fetchMock from 'jest-fetch-mock';
import {render} from '@testing-library/react';

const mockFetchAccessToken = () => fetchMock.mockResponseOnce(() =>
    Promise.resolve(JSON.stringify({
        token_type: 'token_type',
        access_token: 'access_token'
    })));

describe('Login page', () => {
    beforeEach(() => {
        fetchMock.doMock();
    });

    afterEach(() => {
        fetchMock.resetMocks();
    });

    it('render nothing', () => {
        const {container} = render(<LoginPage />);
        expect(container).toBeEmptyDOMElement();
    });

    it('redirect to auth page by default', async () => {
        fetchMock.mockResponseOnce(() => Promise.reject(new Error()));
        expect(await getServerSideProps({query: {}, req: {headers: {}}, res: undefined})).toEqual({
            redirect: {
                destination: authUrl,
                permanent: false
            }
        });
    });

    it('redirect to home page when authentificated', async () => {
        const mockResponse = JSON.stringify({
            resource: {
                data: {
                    userId: 'id',
                    username: 'Username',
                    email: 'email@email.email'
                }
            }
        });
        fetchMock.mockResponseOnce(() => Promise.resolve(mockResponse));
        const mockCookies = cookie.serialize(FAUNA_SECRET_COOKIE, 'some-secret');
        const mockRequest = {headers: {cookie: mockCookies}};
        expect(await getServerSideProps({query: {}, req: mockRequest, res: undefined})).toEqual({
            redirect: {
                destination: '/',
                permanent: false
            }
        });
    });

    it('redirect to home after login', async () => {
        // Get access token
        mockFetchAccessToken();
        // Get user info
        fetchMock.mockResponseOnce(() => Promise.resolve(JSON.stringify({
            id: 'id',
            username: 'Username',
            email: 'email@email.email',
            verified: true
        })));
        // Skip user creation
        fetchMock.mockResponseOnce(() => Promise.resolve(JSON.stringify({})));
        // Get user secret
        fetchMock.mockResponseOnce(() => Promise.resolve(JSON.stringify({
            resource: {secret: 'some-secret'}
        })));

        const mockRequest = {headers: {cookie: undefined}};
        const mockQuery = {code: 'some-code'};
        expect(await getServerSideProps({
            query: mockQuery,
            req: mockRequest,
            res: {setHeader: jest.fn()}
        })).toEqual({
            redirect: {
                destination: '/',
                permanent: false
            }
        });
    });

    it('redirect to home page with error when an error occured', async () => {
        mockFetchAccessToken();
        // Get user info
        fetchMock.mockResponseOnce(() => Promise.resolve(JSON.stringify({
            id: 'id',
            username: 'Username',
            email: 'email@email.email',
            verified: false
        })));
        expect(await getServerSideProps({query: {code: 'some-code'}, req: {headers: {}}, res: undefined})).toEqual({
            redirect: {
                destination: '/?error=Error%3A+Cannot+identify+unverified+user',
                permanent: false
            }
        });
    });
});
