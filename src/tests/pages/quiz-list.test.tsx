import {SetupTestContext, SetupTestContextProps} from '../../utils/test/setup-test-context';
import {render, waitFor} from '@testing-library/react';

import {Quiz} from '@models/quiz';
import QuizList from '../../pages/quiz-list';
import {SWRConfig} from 'swr';
import fetchMock from 'jest-fetch-mock';

const mockQuizList: Quiz[] = [
    {id: '1', name: 'first', questions: []},
    {id: '2', name: 'second', questions: []}
];

const setupTest = (contextProps: SetupTestContextProps = {}) => render(
    <SetupTestContext
        {...contextProps}
        hasRouter
        hasAuth
        hasTheme>
        <SWRConfig value={{dedupingInterval: 0}}>
            <QuizList />
        </SWRConfig>
    </SetupTestContext>
);

describe('QuizList page', () => {
    beforeEach(() => {
        fetchMock.resetMocks();
    });

    it('need authentification', async () => {
        const redirectSpy = jest.fn();
        setupTest({mockedRouterProps: {push: redirectSpy}});
        await waitFor(() => expect(redirectSpy).toHaveBeenCalled());
    });

    it('render with quiz list', async () => {
        fetchMock.doMockIf(/\/api\/quiz-list/, () => Promise.resolve(JSON.stringify(mockQuizList)));
        const {getByText} = setupTest({authUser: {userId: 'id', username: 'Username', email: 'email@test.com'}});
        await waitFor(() => {
            expect(getByText(mockQuizList[0].name));
            expect(getByText(mockQuizList[1].name));
        });
    });

    it('render without quiz list', async () => {
        fetchMock.doMockIf(/\/api\/quiz-list/, () => Promise.resolve(JSON.stringify([])));
        const {getByText} = setupTest({authUser: {userId: 'id', username: 'Username', email: 'email@test.com'}});
        await waitFor(() => expect(getByText('No quiz found')));
    });
});
