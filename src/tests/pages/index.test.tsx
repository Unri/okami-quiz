import {SetupTestContext, SetupTestContextProps} from '../../utils/test/setup-test-context';
import {render, waitFor} from '@testing-library/react';

import Home from '../../pages/index';
import fetchMock from 'jest-fetch-mock';
import userEvent from '@testing-library/user-event';

const setupTest = (contextProps: SetupTestContextProps = {}) => render(
    <SetupTestContext
        {...contextProps}
        hasRouter
        hasAuth
        hasTheme>
        <Home />
    </SetupTestContext>
);

describe('Home page', () => {
    beforeAll(() => {
        fetchMock.doMockIf(/\/api\/auth/, () => Promise.resolve(JSON.stringify({
            userId: 'id',
            username: 'Username',
            email: 'email@email.email'
        })));
    });

    it('has an input to enter a string code', async () => {
        const {getByPlaceholderText} = setupTest();
        await waitFor(() =>
            expect(getByPlaceholderText('Code PIN du quiz')).toBeInTheDocument()
        );
    });

    it('has a button to submit the room code', async () => {
        const {getByRole} = setupTest();

        await waitFor(() =>
            expect(getByRole('button', {name: 'REJOINDRE', hidden: false})).toBeInTheDocument()
        );
    });

    it('show an error if one is passed in query string', async () => {
        const errorMessage = 'Some error message';
        const {getByText, queryByText} = setupTest({mockedRouterProps: {query: {error: errorMessage}}});

        await waitFor(() =>
            expect(getByText(errorMessage)).toBeInTheDocument()
        );
        // Click somewhere else than the error message to close it
        userEvent.click(getByText('Okami quiz'));
        await waitFor(() =>
            expect(queryByText(errorMessage)).not.toBeInTheDocument()
        );
    });

    it('can logout from home page', async () => {
        fetchMock.doMockIf(/\/api\/logout/, () => Promise.resolve(JSON.stringify(true)));
        const {getByRole} = setupTest({authUser: {
            userId: 'id',
            username: 'username',
            email: 'email@email.com'
        }});
        userEvent.click(getByRole('button', {name: 'Déconnexion'}));

        await waitFor(() =>
            expect(getByRole('button', {name: 'Connexion'})).toBeInTheDocument()
        );
    });
});
